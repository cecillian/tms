import firebase from 'firebase'
import 'firebase/firestore'

const config = {
    apiKey: "AIzaSyAdBC8TannBVSXwwQfDTWpjQ81vNmLpsuw",
    authDomain: "gazer-gm001.firebaseapp.com",
    databaseURL: "https://gazer-gm001.firebaseio.com",
    projectId: "gazer-gm001",
    storageBucket: "gazer-gm001.appspot.com",
    messagingSenderId: "636713889194"
  }


  const firebaseapp = firebase.initializeApp(config)

  export default firebaseapp.firestore()